const path = require('path');
const axios = require('axios');
const Sequelize = require('sequelize');
const express = require('express');
const app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const sequelize = new Sequelize('sequelize_artist', 'api_user', null, {
  dialect: 'sqlite',
  define: {
    timestamps: false
  },
  storage: './database.sqlite'
});

sequelize
  .authenticate()
  .then((err) => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.log('Unable to connect to the database:', err);
  });

const Artist = sequelize.define('artist', {
  name: {
    type: Sequelize.STRING,
    primaryKey: true,
    allowNull: false
  },
  rating: Sequelize.INTEGER
}, {
  underscored: true
});

const Drawing = sequelize.define('drawing', {
  title: Sequelize.STRING,
  category: Sequelize.STRING,
  url: {
    type: Sequelize.TEXT,
    primaryKey: true,
    allowNull: false
  },
  likes: Sequelize.INTEGER,
  artist_fk: {
    type: Sequelize.STRING,
    references: {
      model: Artist,
      key: 'name'
    }
  }
});

// Artist.hasMany(Drawing);
// Drawing.belongsTo(Artist);

sequelize
  .sync({
    // false - do not rewrite db
    // true - rewrite db
    force: true
  })
  .then((err) => {
    console.log('It worked!');
  })
  .catch((err) => {
    console.log('An error occurred while creating the table:', err);
  });

const apiGetter = async function() {
  const AuthStr = await axios.get('https://www.deviantart.com/oauth2/token?client_id=9034&client_secret=2c6d5015721baaf9115781e61b6ef3c8&grant_type=client_credentials&code=code')
    .then(response => {
      return 'Bearer ' + response.data.access_token;
    })
    .catch(error => {
      console.log(error);
    });

  const data = await axios.get('https://www.deviantart.com/api/v1/oauth2/browse/newest', {
      // const data = await axios.get('https://www.deviantart.com/api/v1/oauth2/browse/user/journals?username=wendigomoon', {
      headers: {
        Authorization: AuthStr
      }
    })
    .then(response => response.data.results)
    .catch(error => {
      console.log('error ' + error);
    });

  return await data;
}

app.use('/assets', express.static('assets'));

app.get('/find', async (req, res) => {
  const selected = await Drawing.find({
    where: {
      url: req.query.url
    }
  });

  // for testing
  // await console.log(selected.dataValues);

  res.status(200).json(selected.dataValues);
});

app.get('/find_artist', async (req, res) => {
  const selected = await Artist.find({
    where: {
      name: req.query.name
    }
  });

  // for testing
  // await console.log(selected.dataValues);

  res.status(200).json(selected.dataValues);
});

app.get('/like', async (req, res) => {
  // for testing
  console.log(req.query.url);

  const selected = await Drawing.find({
    where: {
      url: req.query.url
    }
  });

  // for testing
  await console.log(selected.dataValues);

  await Drawing.update({
    likes: selected.dataValues.likes + 1,
  }, {
    where: {
      url: req.query.url
    }
  });

  // for testing
  const selected2 = await Drawing.find({
    where: {
      url: req.query.url
    }
  });

  // for testing
  await console.log(selected2.dataValues);

  const selectedArtist = await Artist.find({
    where: {
      name: selected.dataValues.artist_fk
    }
  });

  await Artist.update({
    rating: selectedArtist.dataValues.rating + 1
  }, {
    where: {
      name: selected.dataValues.artist_fk
    }
  });

  // for testing
  const selectedArtist2 = await Artist.find({
    where: {
      name: selected.dataValues.artist_fk
    }
  });

  // for testing
  await console.log(selectedArtist2.dataValues);
  res.status(200).json({
    lol: "haha"
  });
});

app.get('/unlike', async (req, res) => {
  // for testing
  console.log(req.query.url);

  const selected = await Drawing.find({
    where: {
      url: req.query.url
    }
  });

  // for testing
  await console.log(selected.dataValues);

  await Drawing.update({
    likes: selected.dataValues.likes - 1,
  }, {
    where: {
      url: req.query.url
    }
  });

  // for testing
  const selected2 = await Drawing.find({
    where: {
      url: req.query.url
    }
  });

  // for testing
  await console.log(selected2.dataValues);

  const selectedArtist = await Artist.find({
    where: {
      name: selected.dataValues.artist_fk
    }
  });

  await Artist.update({
    rating: selectedArtist.dataValues.rating - 1
  }, {
    where: {
      name: selected.dataValues.artist_fk
    }
  });

  // for testing
  const selectedArtist2 = await Artist.find({
    where: {
      name: selected.dataValues.artist_fk
    }
  });

  // for testing
  await console.log(selectedArtist2.dataValues);
  res.status(200).json({
    lol: "haha"
  });
});

app
  .use(express.static('./public'))
  .get('/', async (req, res) => {
    res.sendFile(path.join(__dirname, './public/index.html'));
  });

app.get('/db', async (req, res) => {

  const data = await apiGetter();
  const drawings = [];
  const artists = [];

  await data.forEach(elem => {
    const newDrawing = {
      url: elem.content.src,
      title: elem.title,
      category: elem.category,
      likes: 0,
      artist_fk: elem.author.username
    };

    const newArtist = {
      name: elem.author.username,
      rating: 0
    };

    drawings.push(newDrawing);
    !artists.find(artist => artist.name === newArtist.name) && artists.push(newArtist);
  });

  await console.log(drawings);
  await console.log(artists);

  try {
    await Artist.bulkCreate(artists);
    console.log("Artists added");

    await Drawing.bulkCreate(drawings);
    console.log("Drawings added");
  } catch (e) {
    console.warn(e);
  }

  // for app
  // http://localhost:8080
  // for like
  // http://localhost:8080/like?url=https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/intermediary/f/f26b10c8-1b21-4c4a-bba8-037ec19e4dd2/dcuiga4-7d4dca17-1052-4a06-a97d-41360df70613.png

  // for testing
  let test = await Artist.findAll();
  await test.forEach(artist => {
    console.log(artist.dataValues);
  });

  // for testing
  let test2 = await Drawing.findAll();
  await test2.forEach(drawing => {
    console.log(drawing.dataValues);
  });

  const drawingsDB = await Drawing.findAll();
  const artistsDB = await Artist.findAll();
  const fullDB = {
    drawings: drawingsDB,
    artists: artistsDB
  }
  console.log(fullDB);
  res.json(fullDB);
});

const server = app.listen(5000, function () {
  const host = server.address().address;
  const port = server.address().port;
  console.log('Example app listening at http://%s:%s', host, port);
});
