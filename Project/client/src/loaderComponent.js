import React, {Component} from 'react';
import axios from 'axios';
import Posts from './posts';
import'./loaderComponent.css';

 class LoaderComponent extends Component {

   constructor(props) {
       super(props);
       this.state = {
         /* initial state */
         artists: [],
         drawings: [],
         loaded: false
       };
     }

  componentDidMount() {
    var _this = this;

    this.serverRequest = axios
      .get("http://localhost:5000/db")
      .then(function(result) {
        _this.setState({
          artists: result.data.artists,
          drawings: result.data.drawings,
          loaded: true
        });
        console.log(result.data);
      })
  }

  showResult(){
    console.log(this.state.artists);
    console.log(this.state.drawings);
  }

  componentWillUnmount() {
    this.serverRequest.abort();
  }

  render() {
    let data = null;;
    if (this.state.loaded){
      this.showResult();
      data =  <Posts onClick={() => this.componentDidMount()} drawings = {this.state.drawings} artists = {this.state.artists} />
    }
    else {
      data = <div className="cssload-container">
	             <ul className="cssload-flex-container">
              		<li>
              			<span className="cssload-loading"></span>
              		</li>
                  </ul>
              	</div>
              }
    return (
      <div className="loaderComponent">
       {data}
      </div>
    )
  }
};

export default LoaderComponent ;
