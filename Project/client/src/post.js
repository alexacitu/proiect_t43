import React from 'react';
import Likes from './likes';
import './post.css';

const post = props => {
  return (
    <div className="drawing">
      <div className="drawing-photo">
        <img src={props.drawing.url} alt="drawing"/>
      </div>
      <div className="drawing-info">
        <div className="title">{props.drawing.title} </div>
        <div className="category">Category: {props.drawing.category} </div>
        <Likes likesNumber = {props.drawing.likes} artistName = {props.artist.name} artistRating = {props.artist.rating} url = {props.drawing.url}>
        </Likes>
      </div>
    </div>
  )
}

export default post;
