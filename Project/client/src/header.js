import React, {Component} from 'react';
import './header.css'

class Header extends Component {

  render() {
    return (
      <header>
          <div className="navigation">
            <div className="logo">
              DeviantArt&nbsp;Project
            </div>
            <nav>
              <a href="https://www.deviantart.com/" target="_blank" rel="noopener noreferrer">
                <div className="color-border">
                  <span className="centered-text">
                    DeviantArt
                  </span>
                </div>
              </a>
              <a href="https://reactjs.org/" target="_blank" rel="noopener noreferrer">
                <div className="color-border">
                  <span className="centered-text">
                    ReactJS
                  </span>
                </div>
              </a>
              <a href="https://nodejs.org/en/" target="_blank" rel="noopener noreferrer">
                <div className="color-border">
                  <span className="centered-text">
                    NodeJS
                  </span>
                </div>
              </a>
              <a href="http://ase.ro/" target="_blank" rel="noopener noreferrer">
                <div className="color-border">
                  <span className="centered-text">
                    ASE
                  </span>
                </div>
              </a>
            </nav>
          </div>
      </header>);
  }
};



export default Header;
