import React from 'react';
import Post from './post';
import './posts.css';


const posts = (props) => {

    return (  <div className="drawingsList">
                {
                  props.drawings.map((item, i) => {
                    const selectedArtist = props.artists.find(a => a.name === item.artist_fk);
                    return <Post drawing = {item} artist = {selectedArtist} key = {i}/>
                  })
                }
              </div>
            )
    }

export default posts;
