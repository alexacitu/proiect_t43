import React from 'react';
import axios from 'axios';
import './likes.css';

class Likes extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
      url: props.url,
      likes: props.likesNumber,
      artistName: props.artistName,
      artistRating: props.artistRating,
      updated: false
    };

  }

  updateLikes = () => {

    if (!this.state.updated) {
      axios
        .get(`http://localhost:5000/like?url=${this.state.url}`)
        .then(res => {
          console.log(res);
        })
        .catch(err => console.log(err));

      this.setState((prevState, props) => {
        return {
          likes: prevState.likes + 1,
          artistName: prevState.artistName,
          artistRating: prevState.artistRating + 1,
          updated: true
        };
      });

    } else {
      axios
        .get(`http://localhost:5000/unlike?url=${this.state.url}`)
        .then(res => {
          console.log(res);
        })
        .catch(err => console.log(err));

      this.setState((prevState, props) => {
        return {
          likes: prevState.likes - 1,
          artistName: prevState.artistName,
          artistRating: prevState.artistRating - 1,
          updated: false
        };
      });
    }
  }

  render() {
    const label = this.state.updated ? 'Unlike' : 'Like';
    const labelClass = this.state.updated ? 'unlikeBtn' : 'likeBtn';

    return (
      <div className="drawing-footer">
        <div className="artist">
          <div className="artist-name">
            {this.state.artistName}
            <i className="fas fa-heart"></i>
            <span className="artist-rating">{this.state.artistRating}</span>
          </div>
        </div>
        <div className="buttons">
          <button className={labelClass} onClick={this.updateLikes}>
            <i className="fas fa-thumbs-up"></i> {label}
          </button>
          <span> {this.state.likes} </span>
        </div>
      </div>
    );
  }
}

export default Likes;
