import React, {
  Component
} from 'react';
import './App.css';
import LoaderComponent from './loaderComponent';
import Header from './header';

class App extends Component {

  render() {
    return ( <div className = "App">
      <Header/>
      <LoaderComponent/>
    </div>);
  }
};



export default App;
